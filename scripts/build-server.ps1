$projectPath = Resolve-Path -Path "..\src\clearscriptrunner"
$sln = "$projectPath\clearscriptrunner.sln"
$output = Resolve-Path -Path "..\src\clearscript-debugger\server"

Write-Host "Building $sln" 

$msBuild = Find-MsBuild
Write-Host "Found MSBuild at $msBuild" 

Write-Host "Deleting $output"
Get-ChildItem -Path C:\Temp -Include *.* -Recurse | foreach { $_.Delete()}

& $msBuild /m $sln /t:"Rebuild" /p:PipelineDependsOnBuild=false /p:OutputPath=$output /p:Configuration="Debug" 

function global:Find-MsBuild  
{
    Param(
        [parameter(Mandatory=$false)] [int] $MaxVersion = 2017
        )
    

    $agentPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\msbuild.exe"
    $devPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe"
    $proPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe"
    $communityPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe"
    $fallback2015Path = "${Env:ProgramFiles(x86)}\MSBuild\14.0\Bin\MSBuild.exe"
    $fallback2013Path = "${Env:ProgramFiles(x86)}\MSBuild\12.0\Bin\MSBuild.exe"
    $fallbackPath = "C:\Windows\Microsoft.NET\Framework\v4.0.30319"

    If ((2017 -le $MaxVersion) -And (Test-Path $agentPath)) { return $agentPath } 
    If ((2017 -le $MaxVersion) -And (Test-Path $devPath)) { return $devPath } 
    If ((2017 -le $MaxVersion) -And (Test-Path $proPath)) { return $proPath } 
    If ((2017 -le $MaxVersion) -And (Test-Path $communityPath)) { return $communityPath } 
    If ((2015 -le $MaxVersion) -And (Test-Path $fallback2015Path)) { return $fallback2015Path } 
    If ((2013 -le $MaxVersion) -And (Test-Path $fallback2013Path)) { return $fallback2013Path } 
    If (Test-Path $fallbackPath) { return $fallbackPath } 

    throw "Could not locate MS build (check \scripts\lib\_build.ps1 for the relevant code)"
}

