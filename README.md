# ClearScipt Debugger

This extension allows you to quickly and easily debug ClearScript scripts.

** How does it work? **

Once you start debugging a script, a .NET script runner process will be launched that can receive requests to execute ClearScript scripts over HTTP. This process will be closed when you exit Visual Studio Code.  
  
  
![ClearScript Debugging Preview](./images/preview.gif)

## Extension Settings

|Name | Description
|-----|------------
|`clearscript-debugger.debugPort`|The port to use for the debugger. The default port is 9222. 
|`clearscript-debugger.scriptRunnerPort`|The port to run the API on. The default port is 65520. 
|`clearscript-debugger.preExecutionScripts`|Defines the file paths of scripts to run prior to the script being debugged.
|`clearscript-debugger.clearscript.concatenateScripts`|Defines whether the 'preExecutionScripts' are prepended to the script being debugged or run separately.
|`clearscript-debugger.clearscript.allowReflection`|Defines whether script code is permitted to use reflection.
|`clearscript-debugger.clearscript.suppressExtensionMethodEnumeration`|Enables or disables extension method enumeration.
|`clearscript-debugger.clearscript.suppressInstanceMethodEnumeration`|Enables or disables instance method enumeration.
|`clearscript-debugger.clearscript.hostFunctions`|Defines whether host functions are available to script code.
|`clearscript-debugger.clearscript.extendedHostFunctions`|Defines whether extended host functions are available to script code.
|`clearscript-debugger.clearscript.hostObjectNamespaces`|Define root level host objects exposed to script code.

## Setup

Ensure that the script runner port is available. You may need to run the following command: `netsh http add urlacl url=http://+:65520/ user=<YOUR USER>`.

## Known Issues

- If you try and debug a file without a folder open in VS Code, you'll receive a "Path must be a string. Received undefined". Issue is being tracked here: https://github.com/Microsoft/vscode/issues/48574.