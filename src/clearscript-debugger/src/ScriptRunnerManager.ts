import * as request from 'request';
import ConfigManager from './ConfigManager';
import { Logger } from './logger';


export class ScriptRunnerManager {
    constructor(private config: ConfigManager, private logger: Logger) {
    }

    public runScript(documentPath: string): Promise<boolean> {
        let postData = {
            allowReflection: this.config.allowReflection,
            suppressExtensionMethodEnumeration: this.config.suppressExtensionMethodEnumeration,
            hostFunctions: this.config.hostFunctions,
            extendedHostFunctions: this.config.extendedHostFunctions,
            hostObjectNamespaces: this.config.hostObjectNamespaces,
            scriptPath: documentPath,
            debugPort: this.config.debugPort,
            preExecutionScripts: this.config.preExecutionScripts,
            concatenateScripts: this.config.concatenateScripts,
            suppressInstanceMethodEnumeration: this.config.suppressInstanceMethodEnumeration
        };

        let options = {
            method: 'post',
            body: postData,
            json: true,
            url: `http://localhost:${this.config.scriptRunnerPort}/clearscript/_run`
        }
        
        this.logger.appendLine("posting debug request to " + options.url);

        return new Promise((resolve, reject) => {
            request(options, (error, response, body) => {
                if (error || response.statusCode == 500) {
                    error = error ? error : "no error returned";
                    this.logger.appendLine(`debug request responded with ${response.statusCode} and error '${error}'`)
                    reject(error);
                }
                else {
                    resolve(true);
                }
            });
        });
    }
}