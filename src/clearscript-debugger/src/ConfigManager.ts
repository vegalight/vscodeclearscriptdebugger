import * as vscode from 'vscode';

export default class Config {

    private extensionName: string = "clearscript-debugger";

    private _scriptRunnerPath: string;
    private _scriptRunnerPort: number;
    private _debugPort: number;

    constructor() {
        const extension = vscode.extensions.getExtension(`yaron.${this.extensionName}`);
        this._scriptRunnerPath = extension.extensionPath;

        var config = vscode.workspace.getConfiguration('clearscript-debugger');

        this._debugPort = config.get('debugPort') || 9222;
        this._scriptRunnerPort = config.get('scriptRunnerPort') || 65520;
    }

    get scriptRunnerPath(): string {
        return this._scriptRunnerPath;
    }

    get scriptRunnerPort(): number {
        return this._scriptRunnerPort;
    }

    get debugPort(): number {
        return this._debugPort;
    }

    get preExecutionScripts(): string[] {
        return vscode.workspace.getConfiguration(this.extensionName).get('preExecutionScripts');
    }

    get concatenateScripts(): boolean {
        return vscode.workspace.getConfiguration(this.extensionName).get('concatenateScripts');
    }

    get allowReflection(): boolean {
        return vscode.workspace.getConfiguration(this.extensionName).get('clearscript.allowReflection');
    }

    get suppressExtensionMethodEnumeration(): boolean {
        return vscode.workspace.getConfiguration(this.extensionName).get('clearscript.suppressExtensionMethodEnumeration');
    }

    get suppressInstanceMethodEnumeration(): boolean {
        return vscode.workspace.getConfiguration(this.extensionName).get('clearscript.suppressInstanceMethodEnumeration');
    }

    get hostFunctions(): object {
        return vscode.workspace.getConfiguration(this.extensionName).get('clearscript.hostFunctions');
    }

    get extendedHostFunctions(): object {
        return vscode.workspace.getConfiguration(this.extensionName).get('clearscript.extendedHostFunctions');
    }

    get hostObjectNamespaces(): object {
        return vscode.workspace.getConfiguration(this.extensionName).get('clearscript.hostObjectNamespaces');
    }
}