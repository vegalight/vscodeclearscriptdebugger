export class Logger {
    private _writer: (message: string) => void;
    private _debugWriter: (message: string) => void;

    private _indentLevel: number = 0;
    private _indentSize: number = 4;
    private _atLineStart: boolean = false;

    constructor(writer: (message: string) => void, debugWriter: (message: string) => void) {
        this._writer = writer;
        this._debugWriter = debugWriter;
    }

    private _appendCore(message: string, logLevel?: LogLevel): void {
        var prefixedMessage = "clearscript-debugger: " + message;
        var writer = this.getWriter(logLevel);

        if (this._atLineStart) {
            if (this._indentLevel > 0) {
                const indent = " ".repeat(this._indentLevel * this._indentSize);
                writer(indent);
            }

            this._atLineStart = false;
        }

        writer(prefixedMessage); 
    }

    public append(message?: string, logLevel?: LogLevel): void {
        message = message || "";
        this._appendCore(message, logLevel);
    }
    
    public appendLine(message?: string, logLevel?: LogLevel): void {
        message = message || "";
        this._appendCore(message + '\n', logLevel);
        this._atLineStart = true;
    }

    private getWriter(logLevel?: LogLevel): (message: string) => void {
        if (logLevel == LogLevel.Info) {
            return this._writer;
        }

        return this._debugWriter;
    }
}

export enum LogLevel {
    Debug,
    Info
}