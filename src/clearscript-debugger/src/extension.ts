'use strict';

import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';
import * as os from 'os';
import { Logger } from './logger';
import ConfigManager from './ConfigManager';

import { ScriptRunnerProcess } from './ScriptRunnerProcess';
import { ScriptRunnerManager } from './ScriptRunnerManager';
import { Disposable } from 'vscode';

let _channel: vscode.OutputChannel = null;

let _logger: Logger = null;
let _config = new ConfigManager();
let _scriptRunnerManager: ScriptRunnerManager = null;
let _scriptRunnerProcess: ScriptRunnerProcess = null;

export function activate(context: vscode.ExtensionContext) {
    _channel = vscode.window.createOutputChannel("ClearScript Debugger");
    _logger = new Logger(message => _channel.append(message), message => console.log(message));
    _scriptRunnerManager =  new ScriptRunnerManager(_config, _logger);
    _scriptRunnerProcess = new ScriptRunnerProcess(_config.scriptRunnerPath, _config.scriptRunnerPort, _logger);

    let disposable = null;

    let editor = vscode.window.activeTextEditor;
    if (!editor) {
        disposable = registerCommand(() => {
            vscode.window.showErrorMessage("There is no active editor");
        });

    } else {
        disposable = registerCommand(() => {
            run();
        });
    }

    context.subscriptions.push(disposable);
}

export function deactivate() {
    if (_scriptRunnerProcess) {
        _scriptRunnerProcess.stop();
    }
}

export function run() {
    _scriptRunnerProcess.start()
        .then(res => {
            return _scriptRunnerManager.runScript(vscode.window.activeTextEditor.document.fileName);
        })
        .then(res => {
            startDebugging();
        })
        .catch(err => {
            vscode.window.showErrorMessage("Failed to start debugging. Please check the developer console for more information.");
        })
}

export function startDebugging(): void {
    const launchConfiguration = {
        type: "node",
        name: "vscode-clearscript-debugger",
        request: "attach",
        stopOnEntry: false,
        protocol: "inspector",
        port: _config.debugPort
    };

    vscode.debug.startDebugging(undefined, launchConfiguration)
        .then((success) => {
            _logger.appendLine("Started debugger");
        }, (reason) => {
            vscode.window.showErrorMessage(`There's been an error launching the debugger on port ${_config.debugPort}: ${reason}`);
        });
}

export function registerCommand(callback: (...args: any[]) => any): Disposable {
    return vscode.commands.registerCommand('extension.debugClearScript', () => {
        callback();
    });
}