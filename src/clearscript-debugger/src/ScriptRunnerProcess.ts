import * as child from 'child_process';
import * as path from 'path';
import * as fs from 'fs';
import { Logger, LogLevel } from './logger';


export class ScriptRunnerProcess {

    private _path: string;
    private _port: number;
    private _logger: Logger;
    private _scriptRunnerProcess: child.ChildProcess;

    constructor(path: string, port: number, logger: Logger) {
        this._path = path;
        this._port = port;
        this._logger = logger;
    }

    public start(): Promise<boolean> {
        var self = this;
        this._logger.appendLine("starting script runner process");

        return new Promise((resolve, reject) => {
            if (this._scriptRunnerProcess) {
                resolve(true);
                return;
            }

            const serverPath = path.join(this._path, "/server/ClearScriptRunner.exe");

            fs.exists(serverPath, function (exists) {
                if (!exists) {
                    var error = `unable to start server, can't find path at ${serverPath}`;
                    self._logger.appendLine(error);
                    reject(error);
                    return;
                }
                
                 self._logger.appendLine("found script runner server");

                self.startScriptRunnerServer(serverPath)
                    .then(() => {
                        process.on('SIGTERM', () => {
                            this.stop();
                            process.exit(0);
                        });

                        process.on('SIGHUP', () => {
                            this.stop();
                            process.exit(0);
                        });

                        self._scriptRunnerProcess.stdout.on('data', data => {
                            let response = data != null ? data.toString() : "";
                            self._logger.appendLine(`${response}`, LogLevel.Info)
                            process.stdout.write(data);
                        });

                        resolve(true);
                    })
                    .catch(err => {
                        reject(err);
                    });
            });
        });
    }

    public stop(): void {
        if (this._scriptRunnerProcess) {
            this._scriptRunnerProcess.kill('SIGTERM');
        }
    }

    private startScriptRunnerServer(path): Promise<boolean> {
        var self = this;

        return new Promise((resolve, reject) => {
            self._logger.appendLine(`starting server on port ${this._port}`);

            this._scriptRunnerProcess = child.spawn(path, ["-p", this._port.toString()]);
            let out = this._scriptRunnerProcess.stdout;

            this._scriptRunnerProcess.on('error', data => {
                self._logger.appendLine(`error starting server on port ${this._port}`);
                reject();
            });

            this._scriptRunnerProcess.on('close', code => {
                self._logger.appendLine("closing server " + code);

                if (code === 0) {
                    resolve(true)
                }
                else {
                    reject();
                }
            });

            out.on("readable", function () {
                let data = out.read();
                self._logger.appendLine(data);

                if (data != null) {
                    resolve(true);
                }
            });
        });
    }
}