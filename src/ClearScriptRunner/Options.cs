﻿using CommandLine;

namespace ClearScriptRunner
{
    public class Options
    {
        [Option('p', "port", Default = 8080, HelpText = "Specify a port for the API endpoint")]
        public int Port { get; set; }
    }
}
