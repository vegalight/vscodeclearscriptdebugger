﻿using System;
using CommandLine;
using Microsoft.Owin.Hosting;

namespace ClearScriptRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Options>(args)
                .WithParsed(options =>
                {
                    var url = $"http://+:{options.Port}";

                    using (WebApp.Start<Startup>(url))
                    {
                        Console.WriteLine("server running");
                        Console.ReadLine();
                    }
                });

        }
    }
}
