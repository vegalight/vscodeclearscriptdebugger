﻿using System.Collections.Generic;

namespace ClearScriptRunner
{
    public class RunScriptRequest
    {
        public RunScriptRequest()
        {
            DebugPort = 9222;

            HostFunctions = new HostFunctionsOptions();
            ExtendedHostFunctions = new HostFunctionsOptions();
            HostObjectNamespaces = new List<HostObjectNamespaces>();
            PreExecutionScripts = new List<string>();
        }

        public int DebugPort { get; set; }
        public string ScriptPath { get; set; }
        public IEnumerable<string> PreExecutionScripts { get; set; }
        public bool ConcatenateScripts { get; set; }
        public bool AllowReflection { get; set; }
        public bool SuppressExtensionMethodEnumeration { get; set; }
        public bool SuppressInstanceMethodEnumeration { get; set; }
        public HostFunctionsOptions HostFunctions { get; set; }
        public HostFunctionsOptions ExtendedHostFunctions { get; set; }
        public IEnumerable<HostObjectNamespaces> HostObjectNamespaces { get; set; }
    }

    public class HostFunctionsOptions
    {
        public bool Enabled { get; set; }
        public string Name { get; set; }
    }

    public class HostObjectNamespaces
    {
        public string Name { get; set; }
        public IEnumerable<string> Namespaces { get; set; }
    }
}