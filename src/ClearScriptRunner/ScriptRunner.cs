﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.ClearScript;
using Microsoft.ClearScript.V8;

namespace ClearScriptRunner
{
    public static class ScriptRunner
    {
        private static readonly Regex SingleQuoteRegex = new Regex("[\u2018\u2019\u201A]", RegexOptions.Compiled);
        private static readonly Regex DoubleQuoteRegex = new Regex("[\u201C\u201D\u201E]", RegexOptions.Compiled);
        private static readonly Regex EllipsisRegex = new Regex("\u2026", RegexOptions.Compiled);
        private static readonly Regex DashesRegex = new Regex("[\u2013\u2014]", RegexOptions.Compiled);
        private static readonly Regex CircumflexRegex = new Regex("\u02C6", RegexOptions.Compiled);
        private static readonly Regex OpenAngleRegex = new Regex("\u2039", RegexOptions.Compiled);
        private static readonly Regex CloseAngleRegex = new Regex("\u203A", RegexOptions.Compiled);
        private static readonly Regex SpacesRegex = new Regex("[\u02DC\u00A0]", RegexOptions.Compiled);
        private static readonly Regex GreekQuestionMarkRegex = new Regex("[;]", RegexOptions.Compiled);

        public static void Run(RunScriptRequest request, Action<string> logger)
        {
            using (var engine = new V8ScriptEngine(V8ScriptEngineFlags.EnableDebugging | V8ScriptEngineFlags.AwaitDebuggerAndPauseOnStart, request.DebugPort))
            {
                if (request.HostFunctions.Enabled)
                {
                    engine.AddHostObject(request.HostFunctions.Name ?? "host", new HostFunctions());
                }

                if (request.ExtendedHostFunctions.Enabled)
                {
                    engine.AddHostObject(request.ExtendedHostFunctions.Name ?? "xhost", new ExtendedHostFunctions());
                }

                foreach (var obj in request.HostObjectNamespaces)
                {
                    engine.AddHostObject(obj.Name, HostItemFlags.GlobalMembers, new HostTypeCollection(obj.Namespaces.ToArray()));
                }

                engine.SuppressInstanceMethodEnumeration = request.SuppressInstanceMethodEnumeration;
                engine.SuppressExtensionMethodEnumeration = request.SuppressExtensionMethodEnumeration;
                engine.AllowReflection = request.AllowReflection;

                if (request.ConcatenateScripts)
                {
                    var script = GetScript(request.ScriptPath);
                    if (request.PreExecutionScripts.Any())
                    {
                        var preExecutionScripts = request.PreExecutionScripts
                            .Select(GetScript);

                        script = string.Join("\n\n", preExecutionScripts) + "\n\n" + script;
                    }

                    Execute(engine, script, logger);
                }
                else
                {
                    foreach (var preExecutionScript in request.PreExecutionScripts)
                    {
                        Execute(engine, preExecutionScript, logger);

                    }
                    Execute(engine, request.ScriptPath, logger);
                }
            }
        }

        private static void Execute(ScriptEngine engine, string path, Action<string> logger)
        {
            try
            {
                var script = GetScript(path);

                logger($"executing script {path}");

                engine.Execute(script);
            }
            catch (Exception e)
            {
                logger($"an error occurred trying to execute {path}");
                logger(e.ToString());
            }
        }

        private static string GetScript(string path)
        {
            var script = File.ReadAllText(path);
            var sanitised = SanitiseScript(script);
            return sanitised;
        }

        private static string SanitiseScript(string script)
        {
            var newScript = script;

            newScript = SingleQuoteRegex.Replace(newScript, "'");
            newScript = DoubleQuoteRegex.Replace(newScript, "\"");
            newScript = EllipsisRegex.Replace(newScript, "...");
            newScript = DashesRegex.Replace(newScript, "-");
            newScript = CircumflexRegex.Replace(newScript, "^");
            newScript = OpenAngleRegex.Replace(newScript, "<");
            newScript = CloseAngleRegex.Replace(newScript, ">");
            newScript = SpacesRegex.Replace(newScript, " ");
            newScript = GreekQuestionMarkRegex.Replace(newScript, ";");

            return newScript;
        }
    }
}
