﻿using System;
using System.Threading.Tasks;
using Nancy;
using Nancy.ModelBinding;

namespace ClearScriptRunner
{
    public class ClearScriptModule : NancyModule
    {
        public ClearScriptModule()
            : base("clearscript")
        {
            Get["/ping"] = _ => "Pong";

            Post["/_run"] = _ =>
            {
                var request = this.Bind<RunScriptRequest>();
                if (request?.ScriptPath == null)
                    return HttpStatusCode.BadRequest;

                Action<string> logger = Console.WriteLine;

                logger("processing " + request.ScriptPath);

                Task.Run(() =>
                {
                    ScriptRunner.Run(request, logger);
                });
                
                return new { };
            };
        }
    }

}
